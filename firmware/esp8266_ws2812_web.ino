/*
 * 
 * The device has follows sensors in configuration:
 * 
 * DHT                  13
 * Waterleak1
 * Waterleak2
 * Summer               12
 * 
 * + timer0             0,5sek for on Summer
 *   timer1             0,5sek for off summer
 *   timer2             1min   for off timer0 and timer1
 *   Timer4             15min  for the temperature/humidity report over MQTT
 *   MQTT               
 * 
 * 
 */

#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <ESP8266mDNS.h>
#include <ESP8266SSDP.h>
#include <Esp.h>
#include <pgmspace.h>
#include "esp8266WiFiStationCtrl.h"

#include "esp8266_neopixel.h"

#define c_temp_size_            800
#define NEOPIXEL_LED_COUNT_     300
#define NEOPIXEL_PIN_           0
#define C_TIMEOUT_SEC           180
#define C_SERVER_PORT           80
#define C_WITH_DOT_IN_LOG       true
#define C_WITH_CAPTIVE          true
#define C_HTTP_OK               200



WebServer* server;
WiFiStationCtrl wifictrl =  WiFiStationCtrl(); 

Adafruit_NeoPixel strip = Adafruit_NeoPixel( NEOPIXEL_LED_COUNT_, NEOPIXEL_PIN_, NEO_RGB + NEO_KHZ800);


//////////////////////////////////////////////////////////////////////////////////////////////////////  

char html_form[]      PROGMEM = "<form action=\"\" name=\"%s\" method=\"post\">";
char html_form_end[]  PROGMEM = "</form>";
char html_checkbox[]  PROGMEM = "<div stale=\"text-align:left;\">%s<input type=checkbox name=%s %s /></div>";
char html_input_txt[] PROGMEM = "<input type=text name=%s value=\"%s\" />";
char html_input_pw[]  PROGMEM = "<input type=text name=%s value=\"%s\" />";
char html_input_num[] PROGMEM = "<input type=number name=%s value=%d size=7 />";



void httpHandleRoot(){

  PGM_P header_txt = PSTR("text/html");

  String html = wifictrl.getHTMLHeader();  
       
  html += wifictrl.getHTMLCSS(); 

  //html += CPinOut::getHTMLAll();
       
  Serial.print( F("\r\nhandleRoot:") );
//server->send(C_HTTP_OK, header_txt, html); return;  
  //*****************************************************************************************

    PGM_P html_part1  = PSTR(
"    <div><h3>Led strip %s.local</h3></div><div><input type=\"color\" name=\"c\" value=\"%s\" onchange=\"document.forms['pick'].submit();\" />\n\
    &nbsp;<span onclick=\"document.forms['pick'].submit();\" style=\"font-size:16pt;\">%s (R:%d G:%d B:%d) [%d]</span></div>\n\ 
"
);

  PGM_P html_part2_4 = PSTR(
"<div style=\"color:#%s; text-align: left;\">\n\
  <input type=range name=\"%s\"\n\
         min=0 max=255 value=\"%03d\" style=\"width:65%%;\">\n\
  <label for=\"%s\">%s</label>\n\
</div>\n\
"
);
  
  PGM_P html_part2_4_end = PSTR("<div ><button onclick=\"document.forms['pick'].submit();\" style=\"margin-bottom:20px;width:70%%;height:30px;\">SEND</button></div>");

  PGM_P txt_rainbow = PSTR("Rainbow:");   PGM_P txt_rb = PSTR("rb");
  PGM_P txt_chksl = PSTR("chksl");
  PGM_P txt_on = PSTR("on");
  PGM_P txt_unchecked = PSTR("unchecked");
  PGM_P txt_checked = PSTR("checked");
  PGM_P txt_r = PSTR("r");PGM_P txt_g = PSTR("g");PGM_P txt_b = PSTR("b");
  PGM_P txt_rbp = PSTR("rbp");

  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////
  
  char temp[c_temp_size_];

  
  Serial.println(F("Client connected"));
  //digitalWrite ( led, 1 );
  
  // data from the colorpicker (e.g. #FF00FF)
  String color = server->arg("c");
  Serial.println("Color: " + color);

  uint8_t red = 0, green = 0, blue = 0; 
  if ( color.length() > 0 )
  {

     int number = (int) strtol( &color[1], NULL, 16);
  
     red = number >> 16;
     green = number >> 8 & 0xFF;
     blue = number & 0xFF;
     Serial.print( F("Set color:" ) );debug_rgb(red, green, blue);Serial.println("");
      
  }

  String sred   = server->arg(txt_r);  Serial.println("Red: " + sred);
  String sgreen = server->arg(txt_g);  Serial.println("Green: " + sgreen);
  String sblue  = server->arg(txt_b);  Serial.println("Blue: " + sblue);

  String schksl = server->arg(txt_chksl);
  Serial.println("chksl:" + schksl );
  if ( schksl.equals(String(txt_on)) ) {
    
    schksl = String(txt_checked);
    red = sred.toInt();
    green = sgreen.toInt();
    blue = sblue.toInt();
    Serial.print( F("Set RGB:" ) );debug_rgb(red, green, blue);Serial.println("");
   
  }
  else
    schksl = String(txt_unchecked);

  
  String srainbow = server->arg(txt_rb); Serial.println("rb " + srainbow);
  if ( ! srainbow.equals(String(txt_on)) ) {
    
    srainbow = String(txt_unchecked);
    rainbow = false;
   
  }
  else {
    
    rainbow = true;
    srainbow = String(txt_checked);
    Serial.print( F("Set rainbow:" ) );
  }
  String srbp = server->arg(txt_rbp);
  rainbow_pause = srbp.toInt();
  Serial.printf_P( PSTR("Rainbow:%d\r\n"), rainbow);
  Serial.printf_P( PSTR("Rainbow-pause:%d\r\n"), rainbow_pause);
  debug_rgb(red, green, blue);
  /*  
  if ( (sred.length() > 0) && (sgreen.length() > 0) && (sblue.length() > 0 )){
   
    red = sred.toInt();
    green = sgreen.toInt();
    blue = sblue.toInt();

    if ( ! (red + green + blue == 0 )){

      snprintf ( temp, _temp_size_, "#%02X%02X%02X", red, green, blue);
      color = String( temp );//'#' + String(red, HEX) + String(green, HEX) + String(blue, HEX);
      Serial.println("Color:" + color);
    }
  } 
  */
  
  // setting the color to the strip 
//  setNeoColor(color);
//    setNeoColorRGB(red, green, blue);


  char clr [8];
  color.toCharArray(clr, 8);

  /////////////////////////////////////////////////////////////////////////////////////////////////////// 
  //temp[0] = 0;
  
  //snprintf_P ( temp, c_temp_size_, get_html_header(), wifictrl.getHostName().c_str() );
  //String txt = String( get_html_header1() );

//  html += temp;
  delay(10);


  snprintf_P ( temp, c_temp_size_, html_form, "pick" );
  html += temp;

  snprintf_P ( temp, c_temp_size_, html_part1, wifictrl.getHostName().c_str(), color.c_str(), clr, red, green, blue, ((red*256)+green)*256+blue );
  html += temp;
  delay(10);

   
  snprintf_P ( temp, c_temp_size_, html_checkbox, "", txt_chksl , schksl.c_str()  );
  html += temp;

  snprintf_P ( temp, c_temp_size_, html_part2_4, "ff0000", txt_r, red, txt_r, "Red" );
  html += temp;

  snprintf_P ( temp, c_temp_size_, html_part2_4, "00ff00", txt_g, green, txt_g, "Green" );
  html += temp;
  delay(10);

  snprintf_P ( temp, c_temp_size_, html_part2_4, "0000ff", txt_b, blue, txt_b, "Blue" );
  html += temp;

  snprintf_P ( temp, c_temp_size_, html_checkbox, txt_rainbow, txt_rb , srainbow.c_str()  );
  html += temp;

  snprintf_P ( temp, c_temp_size_, html_part2_4, "0000ff", txt_rbp, rainbow_pause, txt_rbp, "" );
  html += temp;
  delay(10);

  snprintf_P ( temp, c_temp_size_, html_part2_4_end );
  html += temp;

  snprintf_P ( temp, c_temp_size_, html_form_end );
  html += temp;
 
  //*****************************************************************************************     
  server->send(C_HTTP_OK, header_txt, html);
}

typedef struct{
  uint64_t check1: 1;
  uint64_t check2: 1;
  uint64_t check3: 1;
  uint64_t check4: 1;
  uint64_t rest:   60;
} bits64_t;

typedef union{
  bits64_t bits_;
  uint64_t int_;
} userdata_t;

void hh_check_arguments(bool* a_check1, bool* a_check2, bool* a_check3, bool* a_check4){

  userdata_t ud;
  ud.int_ = wifictrl.getEEPROMUserData(0);
  *a_check1 =   ud.bits_.check1 > 0;
  *a_check2 =   ud.bits_.check2 > 0;
  *a_check3 =   ud.bits_.check3 > 0;
  *a_check4 =   ud.bits_.check4 > 0;

  if (server->hasArg("chk1") )
       *a_check1 = server->arg("chk1").equals("on")
       ;
  if (server->hasArg("chk2") )
       *a_check2 = server->arg("chk2").equals("on")
       ;
  ud.bits_.check1 = (*a_check1);
  ud.bits_.check2 = (*a_check2);
  if (server->hasArg("chk3") )
       *a_check3 = server->arg("chk3").equals("on")
       ;
  if (server->hasArg("chk4") )
       *a_check4 = server->arg("chk4").equals("on")
       ;
  ud.bits_.check3 = (*a_check3);
  ud.bits_.check4 = (*a_check4);
  wifictrl.setEEPROMUserData(0, ud.int_);
}

void httpHandlePinCfg(){

  bool chk1, chk2, chk3, chk4;

  hh_check_arguments( &chk1, &chk2, &chk3, &chk4 );
  
  PGM_P html1 = PSTR(
  "<form method=\"post\" action='/setupsave'>\n\
  <div><input type=checkbox id=1 name=\"chk1\" \n\
"
  );
  
  PGM_P html2 = PSTR(
  "><label for=1>On start first button must be 'on'</label></div>\n\
  <div><input type=checkbox id=2 name=\"chk2\" \n\
"
  );
  
  PGM_P html3 = PSTR(
  "><label for=2>On start second button must be 'on'</label></div>\n\
  <div><input type=checkbox id=3 name=\"chk3\" \n\
"
  );
  
  PGM_P html4 = PSTR(
  "><label for=3>The first button is the switch</label></div>\n\
  <div><input type=checkbox id=4 name=\"chk4\" \n\
"
  );
  
  PGM_P html5 = PSTR(
  "><label for=4>The second buttonis the switch</label></div>\n\
  <div><input type=\"submit\" value=\"Save\"></div>\n\
  </form></body></html>\n\
"
  );

  PGM_P header_txt = PSTR("text/html");
  PGM_P html_checked = PSTR("checked");
  
  String html = wifictrl.getHTMLHeader();
  html += wifictrl.getHTMLCSS();
  html += html1;
  if ( chk1 )  
    html += html_checked;
  html += html2;
  if ( chk2 )  
    html += html_checked;
  html += html3;
  if ( chk3 )  
    html += html_checked;
  html += html4;
  if ( chk4 )  
    html += html_checked;
  html += html5;
  
  server->send(C_HTTP_OK, header_txt, html);  
}

void httpHandlePinCfgSave(){
  PGM_P header_txt = PSTR("text/html");
  PGM_P html = PSTR("SAVED");

  bool chk1, chk2, chk3, chk4;
  hh_check_arguments( &chk1, &chk2, &chk3, &chk4 );
  
  wifictrl.write2EEPROM();

  server->send(200, header_txt, html);  
}

//////////////////////////////////////////////////////////////////////////////////////////////////////  

void on_wificonnect(WiFiStationCtrl* wifisc){

  
}
//////////////////////////////////////////////////////////////////////////////////////////////////////  


void setup() {

    userdata_t ud;
  ud.int_ = wifictrl.getEEPROMUserData(0);
  //isStartOn[0] =   ud.bits_.check1 > 0;
  //isStartOn[1] =   ud.bits_.check2 > 0;
  //isSwitch[0] =   ud.bits_.check3 > 0;
  //isSwitch[1] =   ud.bits_.check4 > 0;

  /*
  if ( &isSwitch[0] )
    relay0.setRelayOn();
  else
    relay0.setRelayOff();
  if ( &isSwitch[1] )
    relay2.setRelayOn();
  else
    relay2.setRelayOff();
  */

  
  delay(1);
  Serial.begin(115200);

  wifictrl.setOnWiFiConnect( &on_wificonnect );
  


  //btn4.setOnEventPin( &btn_click );
  //btn5.setOnEventPin( &btn_click );

  delay(10);

  //Serial.printf_P( PSTR("sDevName=%s timer1=%s\r\n"), CProjObj::getChipName().c_str(), timer1.getChipName().c_str() ); 
 
  server = new WebServer( C_SERVER_PORT );
  server->begin( );
  server->on("/", httpHandleRoot);
  server->on("/setup", httpHandlePinCfg);
  server->on("/setupsave", httpHandlePinCfgSave);
  
  wifictrl.webServerSetup( server );
  wifictrl.setTimeout( C_TIMEOUT_SEC ); // if SSID haven't active then start Soft access point on the 180 sec.
  wifictrl.setup( C_WITH_DOT_IN_LOG, C_WITH_CAPTIVE ); 

  //##############################

}

static bool temp_report_on_startup = false;

void loop() {
  
  //CProgObj::global_loop();
  
  delay(1); 

  server->handleClient();
  delay(1); 

  //loop_check_wifi_for_led();
  /*
  if ( mqtt.getMQTTClient()->connected() )
    if ( ! temp_report_on_startup ){

        temp_report_on_startup = true;
        mqtt.sendMQTT(&relay0);
        mqtt.sendMQTT(&relay2);
    }*/
}
