#ifndef _ESP8266_NEOPIXEL_H_
#define _ESP8266_NEOPIXEL_H_

#include <Adafruit_NeoPixel.h>


void setNeoColorRGB(uint8_t r, uint8_t g, uint8_t b);
void setNeoColorRainbow();
void loop_neopixel();
void debug_rgb(uint8_t r, uint8_t g, uint8_t b);
void setup_neopixel1(Adafruit_NeoPixel* astrip);

extern Adafruit_NeoPixel strip;
extern int               rainbow_pause;
extern bool              rainbow;
extern uint8_t           colorred,colorgreen,colorblue;
extern int               brightness;

#endif
