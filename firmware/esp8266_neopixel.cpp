#include <pgmspace.h>
//#include "esp8266_types.h"

#include "esp8266_neopixel.h"
#include <Adafruit_NeoPixel.h>



void setup_neopixel1(Adafruit_NeoPixel* astrip){

  // ##############
  // NeoPixel start
  Serial.println();
  (*astrip).setBrightness(brightness);
  (*astrip).begin();
  //strip.show(); 
  //int ic = (rtcData.ss.esp_cfg.color[0] << 16) + (rtcData.ss.esp_cfg.color[1] << 8) + rtcData.ss.esp_cfg.color[2];
  //setNeoColor(String(ic, HEX));
  setNeoColorRGB(0x20, 0x20, 0xCF);
  delay(50);
  Serial.println(F("Started"));

  
}

void debug_rgb(uint8_t r, uint8_t g, uint8_t b){
  // DEBUG
  char temp[200];

  PGM_P debug_rgb = PSTR("RGB:%d %d %d  Striplength: %d");
  snprintf_P (temp, 200, debug_rgb, r, g, b, 300 ); 
  Serial.println(temp);

  
}

int brightness = 150;

int rainbow_pause = 0;
bool rainbow = false;

uint8_t colorred,colorgreen,colorblue;

void setNeoColorRGB(uint8_t r, uint8_t g, uint8_t b){

  debug_rgb(r, g, b);
  colorred = r; colorgreen = g; colorblue = b;

  for(int i = 0; i < 300; i++) {
  //for(int i = 0; i < 60; i++) {
      
    strip.setPixelColor(i, strip.Color( g, r, b ) );
    
  }
  // init
  strip.show();
  
}

void setNeoColorRainbow(){

  randomSeed(random(255));

  uint8_t r = random(0, 255), g = random(0, 255), b = random(0, 255);

  for(int i = 0; i < 300; i++) {

    randomSeed(random(255));
    r = random(0, 255); randomSeed(random(255));
    g = random(0, 255); randomSeed(random(255));
    b = random(0, 255); randomSeed(random(255));
     
    strip.setPixelColor(i, strip.Color( g, r, b ) );
    
  }
  // init
  strip.show();


/*
  int fadeVal=0, fadeMax=100;
  for(uint32_t firstPixelHue = 30; firstPixelHue < 2 *65536;
    firstPixelHue += 256) {

    for(int i=0; i<strip.numPixels(); i++) { // For each pixel in strip...

      uint32_t pixelHue = firstPixelHue + (i * 65536L / strip.numPixels());

      strip.setPixelColor(i, strip.gamma32(strip.ColorHSV(pixelHue, 255,
        255 * fadeVal / fadeMax)));
    }
   }

    strip.show();*/

/*    if(firstPixelHue < 65536) {                              // First loop,
      if(fadeVal < fadeMax) fadeVal++;                       // fade in
    } else if(firstPixelHue >= ((rainbowLoops-1) * 65536)) { // Last loop,
      if(fadeVal > 0) fadeVal--;                             // fade out
    } else {
      fadeVal = fadeMax; // Interim loop, make sure fade is at max
    }
  }

      strip.fill(strip.Color(0, 0, 0, strip.gamma8(j)));
      strip.show(); */
  
  
}

uint32_t counter = 0, per = 0, loctimer_cfreq = 0;
uint8_t  cnt8 = 0;

void loop_neopixel(){

  if ( rainbow )
    if ( rainbow_pause > 0 ) {
    
      counter = millis() / 200;

      per = 255 - rainbow_pause;

      if ( abs((long int)(counter - loctimer_cfreq)) > per ) {

        loctimer_cfreq = counter;
        setNeoColorRainbow();
      
      }

    }
  
}
