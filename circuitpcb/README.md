### Circuit

![Main circuit](../img/ws2812-ESP01-sch.JPG)

1. The power could be done with two way
- from external AC/DC adapter out 5-12V (Power-Jack 5-12V)
- from module TP4056 for accu or any AC/DC adapter out 5V over Micro-USB connector.

2. The LED-Control MOSFET N-Ch must be set only one with case TO-92 or SOT-23.

3. The switch SW1 used for the power control.

4. The button S1 used for reset.

### Board

![Board Top](../img/esp01-ws2812-brd-t.JPG)

![Board Bottom](../img/esp01-ws2812-brd-b.JPG)