This is a little project basen on a ESP-01 (SoC Module from the ESP8266 family). A controller board release a WEB-interface for a controls of the WS2812B based LED-strip or chain of strips.

The board could by supplied from tho sources:
- you can  add a elemets Jack-Plug connector, the voltage regulator like LD33 or LM317 for dropdown input current to 5 Volts.

- you can append lythium-battery-charg module based on the TP4056 (like this  https://ae01.alicdn.com/kf/HTB1_O7dRhjaK1RjSZKzq6xVwXXaf/5pcs-Micro-USB-5V-1A-18650-TP4056-Lithium-Battery-Charger-Module-Charging-Board-With-Protection-Dual.jpg_220x220xz.jpg_.webp)
  and get supply from usb charge device, usb powerbank or directly from lithum battery any type (18560 or a accu from smartphone).
  
After start the control boards you can reach it by address 192.168.4.1 and change all settings in the browser with URL "http://192.168.4.1/wificfg". On this page you can set up the 

- name of the SSID your local router and PSK key;

After settings you need to submit the pache and to restart device.

The control page can you reach with follows URL "http://<yourIPaddress>". On this page can you 

- set the LED colors over colors palette;

- set the color as a mixing of red, green or blue saturation;

- set rainbow colors with changes from 500mS to 5 min.

## Start and setup the new device after first burning

if the device havn't correct parameters like SSID/PSK pair then started automatically Access Point(AP). The SSID of Access Point is some like this IOT<number>. The PSK key URL http://192.168.4.1/, choice the yout SSID, enter the PSK key and press button SAVE.

![Main circuit](img/Screenshot_20211218_151547.jpg)

If you will later change the WiFi setting, you need to open http://ip/wificfg and repeat the recistration.

## Control the device over WEB

The default page of device looked like this:

![0 setting](img/Screenshot_20211220_143654.jpg)
![purple](img/IMG_20211218_194632.jpg)

You can change the color of LED with dialog

![coice color with dialog](img/Screenshot_20211218_190505.jpg)
![turkise](img/IMG_20211218_194741.jpg)

or with slicers

![collor cheise after dialog](img/Screenshot_20211218_190523.jpg)
![rainbow](img/IMG_20211218_194818.jpg)

or activate the rainbow color. In this way changed the slicer the time of chaising.


## Communication with Domoticz

for communication you can use follow script in the ***$(domoticz)/scripts*** folder

```
#!/bin/sh

startup_path=$1
hardware_id=$2
device_id=$3
status=$4
status2=$5
devname=$6

#echo "startup_path=${startup_path}, hardware_id=${hardware_id}, device_id=${device_id}, status=${status}, status2=${status2}, devname=${devname}, p7=${7}, p8=$8 p9=$9"

curl  -s 'http://127.0.0.1:801/json.htm?type=devices&rid=2' > /tmp/dev2.json

#export red=$(`cat /tmp/dev2.json| jq -r .result[0].Color| jq .r`)
red=$(cat /tmp/dev2.json| jq -r '.result[0].Color'| jq '.r')
green=$(cat /tmp/dev2.json| jq -r '.result[0].Color'| jq '.g')
blue=$(cat /tmp/dev2.json| jq -r '.result[0].Color'| jq '.b')

#echo red:$red
#echo green:$green
#echo blue:$blue


#/home/pi/domoticz/scripts/getstripcolor_dev5

color=$((256*256*$red+256*$green+$blue))
echo $color
echo "curl -s 'http://IOT10019111.local/?r=$red&g=$green&b=$blue&c=$color'" > /tmp/dev2.log

curl -s "http://IOT10019111.local/?c=$color">nul
```

![domoticz](img/esp01-ws2812b-domoticz-setup.JPG)